import axios from "axios";
import { FunctionComponent, useEffect, useState } from "react"
import './Dashboard.css'

export const Dashboard: FunctionComponent = () => {
    const timeKpis = ["Pool", "Pool3", "Pool4", "Lcm", "Solver", "Sheduler"];
    const sizeKpis = ["Demand", "PoolDemand", "SolverDemand"];
    const orderKpis = ["AssignTime", "PickupTime", "CompleteTime"];
    const totalKpis = ["LcmUsed", "PickupDistance"];

    const [stats, setStats] = useState<Stat | undefined>(undefined);

    const credentials = {
        username: 'adm0',
        password: 'adm0'
    }

    function getStats() {
        axios.get('http://localhost:8080/stats', { auth: credentials })
            .then(response => {
                if (response.data) {
                    setStats(response.data);
                }
            });
    }

    useEffect(() => {
        getStats();
        let interval = setInterval(() => getStats(), 60000);
        return () => { clearInterval(interval); }
    }, []);


    function findVal(key: string) {
        return stats?.kpis.find(i => i.name === key)?.int_val;
    }

    function renderTable(theme: string, kpis: string[]) {
        let rows = kpis.map((item, index) => {
            return(
            <div className="row">
                <div className="column">
                    {item}
                </div>        
                <div className="column">
                    {findVal("Avg" + item + theme)}
                </div>        
                <div className="column">
                    {findVal("Max" + item + theme)}
                </div>        
            </div>
            );
        });
        return (
            <>
            <b>{theme}</b>
            <div className="row">
                <div className="column headerColumn">KPI</div>        
                <div className="column headerColumn">AVG</div>        
                <div className="column headerColumn">MAX</div>        
            </div>
            {rows}
            </>
        );
    }

    function renderSmallTable(theme: string, kpis: string[]) {
        let rows = kpis.map((item, index) => {
            return(
            <div className="row">
                <div className="column">{item}</div>
                <div className="column">{findVal(theme + item)}</div>
            </div>
            );
        });
        return (
            <>
            <b>{theme}</b>
            <div className="row">
                <div className="column headerColumn">KPI</div>
                <div className="column headerColumn">Value</div>
            </div>
            {rows}
            </>
        );
    }

    function renderStatusTable(theme: string, data: Kpi[]) {
        let rows = data.map((item, index) => {
            return(
            <div className="row">
                <div className="column">{item.name}</div>
                <div className="column">{item.int_val}</div>
            </div>
            );
        });
        return (
            <>
            <b>{theme}</b>
            <div className="row">
                <div className="column headerColumn">Status</div>
                <div className="column headerColumn">Count</div>
            </div>
            {rows}
            </>
        );
    }

    return (
        <div className="wrapper">
            <div className="main">
                <p>{renderTable("Time", timeKpis)}</p>
                <p>{renderTable("Size", sizeKpis)}</p>
                <p>{renderSmallTable("Total", totalKpis)}</p>
                <p>{renderSmallTable("AvgOrder", orderKpis)}</p>
                <p>{renderStatusTable("Orders", stats ? stats.orders : [])}</p>
                <p>{renderStatusTable("Buses", stats ? stats.cabs : [])}</p>
            </div>
        </div>
    )    
}

export interface Kpi {
    name: string;
    int_val: number;
}

export interface Stat {
    kpis: Kpi[];
    orders: Kpi[];
    cabs: Kpi[];
}